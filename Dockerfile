# STEP 1 build executable binary
FROM golang:alpine as builder

# Install git + SSL ca certificates
RUN apk update
RUN apk add git ca-certificates tzdata alpine-sdk

# Create appuser
RUN adduser -D -g '' appuser

COPY api/ $GOPATH/src/github.com/kochie/mastodonchain/api
COPY lib/ $GOPATH/src/github.com/kochie/mastodonchain/lib
COPY config.json $GOPATH/src/github.com/kochie/mastodonchain/
COPY main.go $GOPATH/src/github.com/kochie/mastodonchain/

WORKDIR $GOPATH/src/github.com/kochie/mastodonchain

# Get dependancies
RUN go get -d -v

# Build the binary
RUN env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o /go/bin/mastodonchain

# STEP 2 build a small image
# Start from scratch
FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd

# Copy our static executable
COPY --from=builder /go/bin/mastodonchain /go/bin/
COPY --from=builder /go/src/github.com/kochie/mastodonchain/config.json /go/bin/config.json

USER appuser

EXPOSE 8080

WORKDIR /go/bin
ENTRYPOINT ["/go/bin/mastodonchain"]