package api

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-redis/redis"

	"github.com/kochie/mastodonchain/lib"
	"github.com/kochie/mastodonchain/lib/markov"
	"github.com/kochie/mastodonchain/lib/social"
	"github.com/mattn/go-mastodon"
)

func getConfig() (*lib.Config, *mastodon.Client, *redis.Client) {
	config := lib.LoadDefaultConfig()
	app := social.GetMastodonApp(&config.Mastodon)
	db := markov.GetConnection(&config.Redis)
	return config, app, db
}

// MastodonTest will check the server connection
func MastodonTest(w http.ResponseWriter, r *http.Request) {
	// config := lib.LoadDefaultConfig()
	// app := lib.GetMastodonApp(&config.Mastodon)
	// fmt.Fprintf(w, "client-id    : %s\n", app.Config.)
	// fmt.Fprintf(w, "client-secret: %s\n", app.ClientSecret)
	fmt.Fprintf(w, "Hello, World!\n")
}

// Beep wil send a test message to the connected account
func Beep() (*mastodon.Status, error) {
	config := lib.LoadDefaultConfig()
	app := social.GetMastodonApp(&config.Mastodon)

	message := []string{"beep", "boop", "toot"}[rand.Intn(3)]
	toot := mastodon.Toot{Status: message}

	status, err := app.PostStatus(context.Background(), &toot)
	if err != nil {
		log.Println(err)
	}
	log.Println(message)
	return status, err
}

// MastodonBeep will beep boop the server
func MastodonBeep(w http.ResponseWriter, r *http.Request) {
	status, err := Beep()
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(status)
}

// ListenOnHome will run a goroutine in the background that will listen to events that happen on the Home list
func ListenOnHome() {
	_, app, _ := getConfig()
	eventChannel, err := app.StreamingUser(context.Background())
	if err != nil {
		log.Println(err)
	}
	go func() {
		for {
			e := <-eventChannel
			switch event := e.(type) {
			case *mastodon.UpdateEvent:
				log.Println("update", event.Status.Content)
				go readToot(event.Status)
			case *mastodon.NotificationEvent:
				log.Println("notification", event.Notification.Status)
				go readToot(event.Notification.Status)
			case *mastodon.DeleteEvent:
				log.Println("delete", event.ID)
			case *mastodon.ErrorEvent:
				log.Println("error", event.Error())
			}
		}
	}()

	log.Println("Listening to home...")
}

// ListenOnPublic will run a goroutine in the background that will listen to events that happen on the Public list
func ListenOnPublic() {
	_, app, _ := getConfig()
	eventChannel, err := app.StreamingPublic(context.Background(), false)
	if err != nil {
		log.Println(err)
	}
	go func() {
		for {
			e := <-eventChannel
			switch event := e.(type) {
			case *mastodon.UpdateEvent:
				log.Println("update", event.Status.Content)
				go readToot(event.Status)
			case *mastodon.NotificationEvent:
				log.Println("notification", event.Notification.Status)
				go readToot(event.Notification.Status)
			case *mastodon.DeleteEvent:
				log.Println("delete", event.ID)
			case *mastodon.ErrorEvent:
				log.Println("error", event.Error())
			}
		}
	}()

	log.Println("Listening to public...")
}

// readToot will read the text out of a toot and add this to the markov chain
func readToot(event *mastodon.Status) {
	words := strings.Split(markov.NormaliseToot(event), " ")
	markov.AddSentence(words)
}

// AddUserStatusesByHandle will search for a user with handle and add all the statuses that a user has made
func AddUserStatusesByHandle(accountHandle string) {
	_, app, _ := getConfig()
	accountList, err := app.AccountsSearch(context.Background(), accountHandle, 1)
	if err != nil {
		log.Println(err)
		return
	}

	account := accountList[0]
	AddUserStatuses(account)
}

// AddUserStatuses will add all the statuses that a user has made
func AddUserStatuses(account *mastodon.Account) {
	_, app, _ := getConfig()
	i := 0
	var pg mastodon.Pagination
	for {
		statuses, err := app.GetAccountStatuses(context.Background(), account.ID, &pg)
		if err != nil {
			log.Println(err)
			return
		}
		for _, status := range statuses {
			readToot(status)
			i++
			fmt.Println(i)
		}
		if pg.MaxID == "" {
			break
		}
	}
}

// GetFollowers will return all the current followers on the bot
func GetFollowers() []*mastodon.Account {
	_, app, _ := getConfig()
	account, err := app.GetAccountCurrentUser(context.Background())

	if nil != err {
		log.Println(err)
		return nil
	}

	var followers []*mastodon.Account
	var pg mastodon.Pagination

	for {
		foll, err := app.GetAccountFollowers(context.Background(), account.ID, &pg)
		if err != nil {
			log.Println(err)
			return nil
		}
		followers = append(followers, foll...)
		if pg.MaxID == "" {
			break
		}
	}

	return followers
}

// GetNewFollowers will return all the new followers of the bot
func GetNewFollowers() []*mastodon.Account {
	_, app, db := getConfig()
	account, err := app.GetAccountCurrentUser(context.Background())

	if nil != err {
		log.Println(err)
		return nil
	}

	var followers []*mastodon.Account
	var pg mastodon.Pagination
	var newFollowerIds []string

	for {
		foll, err := app.GetAccountFollowers(context.Background(), account.ID, &pg)
		if err != nil {
			log.Println(err)
			return nil
		}
		for _, follower := range foll {
			r := db.SIsMember("mastodon_followers", string(follower.ID))
			if err := r.Err(); err != nil {
				fmt.Println(err)
			}

			if res := r.Val(); !res {
				followers = append(followers, follower)
			}
		}
		if pg.MaxID == "" {
			break
		}
	}

	for _, follower := range followers {
		id := string(follower.ID)
		newFollowerIds = append(newFollowerIds, id)
	}

	if len(newFollowerIds) > 0 {
		r := db.SAdd("mastodon_followers", newFollowerIds)
		if err := r.Err(); err != nil {
			log.Println(err)
		}
	}

	return followers
}

// Follow will follow a list of users and add then to the redis cache
func Follow(users ...*mastodon.Account) {
	var newFollowingIds []string

	_, app, db := getConfig()
	for _, user := range users {
		r := db.SIsMember("mastodon_following", string(user.ID))
		if err := r.Err(); err != nil {
			fmt.Println(err)
		}

		if res := r.Val(); !res {
			_, err := app.AccountFollow(context.Background(), user.ID)
			if err != nil {
				log.Println(err)
			}
		}
	}

	for _, follower := range users {
		id := string(follower.ID)
		newFollowingIds = append(newFollowingIds, id)
	}

	if len(newFollowingIds) > 0 {
		r := db.SAdd("mastodon_following", newFollowingIds)
		if err := r.Err(); err != nil {
			log.Println(err)
		}
	}
}

// Talk will make a random sentence
func Talk() string {
	sb := strings.Builder{}

	_, _, db := getConfig()
	current := "$EOL$"

	for {
		r := db.HGetAll(fmt.Sprintf("word:%s", current))
		if err := r.Err(); err != nil {
			log.Println(err)
		}

		totalCount := 0
		connectedWords := r.Val()
		for _, count := range connectedWords {
			if val, err := strconv.Atoi(count); err != nil {
				log.Println(err)
			} else {
				// log.Println(val, totalCount)
				totalCount += val
			}
		}

		// fmt.Println("total count", totalCount)
		chosen := rand.Intn(totalCount)

		paramCount := 0
		for word, count := range connectedWords {
			if val, err := strconv.Atoi(count); err != nil {
				log.Println(err)
			} else {
				paramCount += val
				if chosen < paramCount {
					current = word
					break
				}
			}
		}

		if current == "$EOL$" {
			break
		} else {
			sb.WriteString(current)
			sb.WriteString(" ")
		}
	}

	return sb.String()
}
