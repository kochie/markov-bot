package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/kochie/mastodonchain/lib/markov"

	"github.com/kochie/mastodonchain/api"
	"github.com/kochie/mastodonchain/lib"
)

func main() {
	// Hello world, the web server
	config := lib.FindAndReadConfig()
	lib.StoreDefaultConfig(config)
	lib.ReadEnv()

	helloHandler := func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprint(w, "Hello there, the time is\n", time.Now())
	}

	// api.Beep()

	http.HandleFunc("/hello", helloHandler)
	http.HandleFunc("/test", api.MastodonTest)
	http.HandleFunc("/beep", api.MastodonBeep)
	http.HandleFunc("/readuser", func(w http.ResponseWriter, req *http.Request) {
		q := req.URL.Query()
		go api.AddUserStatusesByHandle(q.Get("user"))
		fmt.Fprint(w, "Done")
	})
	http.HandleFunc("/updatefollowers", func(w http.ResponseWriter, req *http.Request) {
		newFollowers := api.GetNewFollowers()
		for _, newFollower := range newFollowers {
			fmt.Fprintln(w, newFollower.Username)
			go api.AddUserStatuses(newFollower)
		}
		go api.Follow(newFollowers...)
	})
	http.HandleFunc("/talk", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprint(w, api.Talk())
	})

	markov.Connect(&config.Redis)
	api.ListenOnHome()
	api.ListenOnPublic()

	log.Fatal(http.ListenAndServe(":8080", nil))
}
