package social

import (
	"github.com/mattn/go-mastodon"

	"github.com/kochie/mastodonchain/lib"
)

var defaultMastodonConnection *mastodon.Client

// Connect will create a client connection to a Mastodon instance
func Connect(config *lib.MastodonConfig) *mastodon.Client {
	app := mastodon.NewClient(&mastodon.Config{
		Server:       config.ServerURL,
		ClientID:     config.ClientKey,
		ClientSecret: config.ClientSecret,
		AccessToken:  config.AccessToken,
	})

	return app
}

// GetMastodonApp returns the connection to the mastodon instance or creates one
func GetMastodonApp(config *lib.MastodonConfig) *mastodon.Client {
	if defaultMastodonConnection == nil {
		return Connect(config)
	}
	return defaultMastodonConnection
}
