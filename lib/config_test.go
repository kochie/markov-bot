package lib

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnmarshalConfig(t *testing.T) {
	expectedConfig := &Config{
		Redis: RedisConfig{
			Password: "password",
			DBName:   1,
			Hostname: "hostname",
		},
		Mastodon: MastodonConfig{
			ServerURL:    "https://server.url",
			ClientKey:    "aabbcc",
			ClientSecret: "112233",
			AccessToken:  "a1b2c3",
		},
	}

	testJSON := `{
		"redis": {
			"password": "password",
			"dbName": 1,
			"hostname": "hostname"
		},
		"mastodon": {
			"serverURL": "https:\/\/server.url",
			"clientKey": "aabbcc",
			"clientSecret": "112233",
			"accessToken": "a1b2c3"
		}
	}`
	rawConfig := []byte(testJSON)

	actualConfig := UnmarshalConfig(&rawConfig)

	assert.Equal(t, expectedConfig, actualConfig, "should be equal")
}
