package lib

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

const (
	invalidJSON     = "Not a valid json configuration"
	fileNotFound    = "Couldn't find config file"
	noDefaultConfig = "No default config made"
)

var defaultConfig *Config

// RedisConfig contains the database connection information
type RedisConfig struct {
	Password string `json:"password"`
	DBName   int    `json:"dbName"`
	Hostname string `json:"hostname"`
}

// MastodonConfig contains the credentials to connect to a mastodon account
type MastodonConfig struct {
	ServerURL    string `json:"serverURL"`
	ClientKey    string `json:"clientKey"`
	ClientSecret string `json:"clientSecret"`
	AccessToken  string `json:"accessToken"`
	Website      string `json:"website"`
}

// Config contains the configuration parameters for the project
type Config struct {
	Redis    RedisConfig    `json:"redis"`
	Mastodon MastodonConfig `json:"mastodon"`
}

// ReadConfigFile will read a json file and return a byte array of the file
func ReadConfigFile(filename string) *[]byte {
	rawConfig, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(fmt.Sprintf("Couldn't open the file - %s", filename))
	}

	return &rawConfig
}

// UnmarshalConfig will take a raw byte array of the config and convert it to a config object. If the byte array given does not match the properties of the Config structure then the method will panic and fail.
func UnmarshalConfig(rawConfig *[]byte) *Config {
	config := &Config{}

	err := json.Unmarshal(*rawConfig, config)
	if err != nil {
		log.Fatal(invalidJSON)
	}

	return config
}

// FindConfig will look for the config file in some known locations. It first looks in the directory the binary is being executed in and then looks in `~/.config/`. If neither of these places contains a valid config file the method will return an error.
func FindConfig() (*[]byte, error) {
	configLocation := "./config.json"

	// simple check to see if the file exists and can be read
	if _, err := os.Stat(configLocation); !os.IsNotExist(err) {
		return ReadConfigFile(configLocation), nil
	}

	// usr, err := user.Current()
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// configLocation = fmt.Sprintf("%s/.config/mastodonbot/config.json", usr.HomeDir)
	// if _, err := os.Stat(configLocation); !os.IsNotExist(err) {
	// 	return ReadConfigFile(configLocation), nil
	// }

	return nil, errors.New(fileNotFound)
}

// FindAndReadConfig is a wrapper that will look for and unmarshal the config file using FindConfig and UnmarshalConfig
func FindAndReadConfig() *Config {
	config, err := FindConfig()

	if err != nil {
		log.Fatal(err)
	}

	return UnmarshalConfig(config)
}

// StoreDefaultConfig will store the config as default
func StoreDefaultConfig(config *Config) {
	defaultConfig = config
}

// LoadDefaultConfig will return the default config if it's defined
func LoadDefaultConfig() *Config {
	if defaultConfig == nil {
		log.Fatal(noDefaultConfig)
	}
	return defaultConfig
}

// ReadEnv will read the env variables and overwrite the config if necessary.
func ReadEnv() {
	if val := os.Getenv("REDIS_HOSTNAME"); val != "" {
		defaultConfig.Redis.Hostname = val
	}
}
