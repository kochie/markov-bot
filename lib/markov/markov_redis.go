package markov

import (
	"fmt"
	"log"
	"strings"

	"github.com/go-redis/redis"
	"github.com/mattn/go-mastodon"
	"golang.org/x/net/html"

	"github.com/kochie/mastodonchain/lib"
)

var defaultRedisConnection *redis.Client

// Connect will create a connection instance that can talk to the database
func Connect(config *lib.RedisConfig) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     config.Hostname,
		Password: config.Password,
		DB:       config.DBName,
	})

	pong, err := client.Ping().Result()
	fmt.Println(pong, err)

	return client
}

// GetConnection will either return the currently open connection, or it will make a new connection and return that
func GetConnection(config *lib.RedisConfig) *redis.Client {
	if defaultRedisConnection == nil {
		defaultRedisConnection = Connect(config)
	}

	return defaultRedisConnection
}

// AddLink will create a directed link between the two words
func AddLink(start, end string) {
	config := lib.LoadDefaultConfig()
	r := GetConnection(&config.Redis)
	key := fmt.Sprintf("word:%s", start)
	fmt.Println(key, end)
	if err := r.HIncrBy(key, end, 1).Err(); err != nil {
		log.Println(err.Error())
	}
}

// AddSentence will add a list of words to the chain
func AddSentence(sentence []string) {
	if len(sentence) == 0 {
		return
	}

	AddLink("$EOL$", sentence[0])

	for i := 0; i < len(sentence)-1; i++ {
		AddLink(sentence[i], sentence[i+1])
	}

	AddLink(sentence[len(sentence)-1], "$EOL$")
}

// NormaliseToot will extract the text from a toot and return text that can be inputted into the chain
func NormaliseToot(toot *mastodon.Status) string {
	z := html.NewTokenizer(strings.NewReader(toot.Content))

	sb := strings.Builder{}

	for {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			// End of the document, we're done
			return sb.String()
		case tt == html.TextToken:
			t := z.Token()
			sb.WriteString(t.Data)
		}
	}
}
