package markov

// Markov struct holds the methods to create and edit the chain
type Markov interface {
	Connect()
	AddLink()
	RetrieveWords() []string
}
