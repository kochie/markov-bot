package markov

import (
	"testing"

	"github.com/mattn/go-mastodon"

	"github.com/stretchr/testify/assert"
)

func TestNormaliseToot(t *testing.T) {
	s1 := `<p><span class="h-card"><a href="https://melb.social/@markovbot" class="u-url mention">@<span>markovbot</span></a></span> who says testing in prod is a bad idea</p>`

	s2 := `<p><span class="h-card"><a href="https://melb.social/@markovbot" class="u-url mention">@<span>markovbot</span></a></span> who says testing in prod is <span class="h-card"><a href="https://melb.social/@markovbot" class="u-url mention">@<span>kochie</span></a></span> a bad idea</p>`

	status := mastodon.Status{Content: s1}

	expected := "@markovbot who says testing in prod is a bad idea"
	actual := NormaliseToot(&status)

	assert.Equal(t, expected, actual, "should normalise the toot")

	status = mastodon.Status{Content: s2}

	expected = "@markovbot who says testing in prod is @kochie a bad idea"
	actual = NormaliseToot(&status)

	assert.Equal(t, expected, actual, "should normalise the toot")
}
